# Vim syntax file for logfiles

## Description

Vim syntax file to highlight various log files.

This syntax file describes how to highlight:

* Error messages (containing 'error', 'fail', etc. words)
* Warning messages (containing 'warning', 'delete', etc. words )
* Strings ( Double quotes and single quotes )
* Date values in various formats
* Time values in various formats
* Numbers - decimal and hex

This syntax is based on messages.vim - syntax file for highlighting Linux kernel
messages, but it's not bound to positioning of elements (messages.vim requires
your logs to start with Date and time values or it will not highlight anything).

## Details

* `syntax/vim.log` is the actual syntax file
* `ftdetect/vim.log` applies filetype "log" (defined by `syntax/vim.log`) to all `*log` files, and this automatically.

## Install

```shell script
mkdir ~/.vim/ftdetect ~/.vim/syntax
cp <this-repo>/syntax/vim.log ~/.vim/syntax
cp <this-repo>/ftdetect/vim.log ~/.vim/ftdetect 
```

## Thanks

This work is just an adaptation of [vim-log-syntax](https://github.com/dzeban/vim-log-syntax) by [Alex Dzyoba](https://github.com/dzeban)
